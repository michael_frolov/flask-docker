FROM ubuntu:18.04

RUN apt-get update
RUN apt install -y python2.7
RUN apt install -y python-pip

# Latest pip version with Python2 support
RUN pip install pip==20.3.4

ENV DEBIAN_FRONTEND=noninteractive
RUN apt install -y tzdata
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata


ENV PORT=8080
