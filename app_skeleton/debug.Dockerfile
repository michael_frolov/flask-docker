FROM cka3o4nik/flask:2.7_image_work

RUN apt install -y python-babel
RUN apt clean

COPY ./app /app

ADD ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

WORKDIR /app
CMD ["python", "/app/app.py"]
