#!/bin/bash

PORT=8080
docker run -v $PWD/app:/app -p $PORT:$PORT -Pti -e PORT=$PORT -e debug=true -e MAILGUN_SMTP_SERVER=172.18.0.1 -e MAILGUN_SMTP_PORT=1025 -e DOMAIN=0.0.0.0 -a stdin -a stdout -a stderr --rm --name flask -i cka3o4nik/flask:latest $@
