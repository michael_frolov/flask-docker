#!/bin/bash

#gsutil iam ch user:p0we7man@gmail.com:roles/storage.admin gs://fuel-quality-305709_cloudbuild
gcloud builds submit --tag gcr.io/fuel-quality-305709/fuel-quality && \
  gcloud run deploy fuel-quality --image gcr.io/fuel-quality-305709/fuel-quality --allow-unauthenticated --platform managed
