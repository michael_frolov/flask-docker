#!/bin/bash

#docker rm -f fuel-quality

#docker run -v $PWD:/app -p 5000:5000 -Pti  -a stdin -a stdout -a stderr --rm --name fuel-quality --network=fuel-quality -i cka3o4nik/fuel_quality $@
docker run -v $PWD/app:/app -p 8080:8080 -Pti -e debug=true -e DOMAIN=0.0.0.0 -a stdin -a stdout -a stderr --rm --name fuel-quality --network=fuel-quality -i cka3o4nik/fuel_quality:latest $@
